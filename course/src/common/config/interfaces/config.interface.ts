export interface IConfig {
  serverPort: number;
  databaseUrl: string;
  rabbitUrl: string;
  botToken: string;
  tgGroupId: string;
}
