import { IConfig } from './interfaces/config.interface';

import * as dotenv from 'dotenv';
dotenv.config();

export const config: IConfig = {
  serverPort: Number(process.env.SERVER_PORT),
  databaseUrl: process.env.DB_URL,
  rabbitUrl: process.env.RABBIT_URL,
  botToken: process.env.BOT_TOKEN,
  tgGroupId: process.env.TG_GROUP_ID
};
