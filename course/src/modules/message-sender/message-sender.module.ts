import { Module } from '@nestjs/common';
import { RabbitMQService } from './message-sender.service';
import { MessageSenderController } from './message-sender.controller';
import { TelegramService } from './telegram-service';

@Module({
  controllers: [MessageSenderController],
  providers: [RabbitMQService, TelegramService],
})
export class MessageSenderModule {}
