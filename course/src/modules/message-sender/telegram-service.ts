import { Injectable } from '@nestjs/common';
import { config } from 'src/common/config';
import * as TelegramBot from 'telegraf';

@Injectable()
export class TelegramService {
  private readonly token = config.botToken;
  private readonly chatId = config.tgGroupId;
  private bot: TelegramBot.Telegraf;

  constructor() {
    this.bot = new TelegramBot.Telegraf(this.token);
  }

  async sendMessage(message: string) {
    await this.bot.telegram.sendMessage(this.chatId, message)
  }
}
