import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { RabbitMQService } from './message-sender.service';
import { CreateMessageSenderDto } from './dto/create-message-sender.dto';
import { UpdateMessageSenderDto } from './dto/update-message-sender.dto';
import { GrpcMethod } from '@nestjs/microservices';
import { Metadata, ServerUnaryCall } from '@grpc/grpc-js';
import { ResData } from 'src/lib/resData';
import { TelegramService } from './telegram-service';

@Controller('message-sender')
export class MessageSenderController {
  constructor(
    private readonly rabbitService: RabbitMQService,
    private readonly telegramService: TelegramService,
  ) {}

  @GrpcMethod('RabbitService', 'send')
  async sendToRabbitMQ(data: {tag: string, message: string}, metadata: Metadata, call: ServerUnaryCall<any, any>) {
    console.log("run");
    
    await this.rabbitService.connect();
    await this.rabbitService.channel.sendToQueue(
      this.rabbitService.queue,
      Buffer.from(`#${data.tag} ${data.message}`),
    );
    return
  }

  async onModuleInit() {
    await this.rabbitService.connect()
    this.rabbitService.consumeMessages(async (msg) => {
      if (msg !== null) {
        const message = msg.content.toString();
        await this.telegramService.sendMessage(message);
      }
    });
    return
  }


}
