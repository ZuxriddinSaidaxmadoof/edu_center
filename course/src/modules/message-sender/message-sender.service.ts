import { Injectable } from '@nestjs/common';
import * as amqp from 'amqplib';
import { config } from 'src/common/config';

@Injectable()
export class RabbitMQService {
  connection: amqp.Connection;
  channel: amqp.Channel;
  readonly queue = 'my_queue';

  async connect() {
    this.connection = await amqp.connect(config.rabbitUrl);
    
    this.channel = await this.connection.createChannel();
    await this.channel.assertQueue(this.queue);
  }


  async consumeMessages(callback: (msg: amqp.Message | null) => void) {
    await this.channel.consume(this.queue, callback, { noAck: true })
  }
}