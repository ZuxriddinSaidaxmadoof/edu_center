import { PartialType } from '@nestjs/mapped-types';
import { CreateMessageSenderDto } from './create-message-sender.dto';

export class UpdateMessageSenderDto extends PartialType(CreateMessageSenderDto) {}
