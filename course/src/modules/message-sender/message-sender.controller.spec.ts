import { Test, TestingModule } from '@nestjs/testing';
import { MessageSenderController } from './message-sender.controller';
import { MessageSenderService } from './message-sender.service';

describe('MessageSenderController', () => {
  let controller: MessageSenderController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MessageSenderController],
      providers: [MessageSenderService],
    }).compile();

    controller = module.get<MessageSenderController>(MessageSenderController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
