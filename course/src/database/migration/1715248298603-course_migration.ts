import { MigrationInterface, QueryRunner } from "typeorm";

export class CourseMigration1715248298603 implements MigrationInterface {
    name = 'CourseMigration1715248298603'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "course-file" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "last_update_at" TIMESTAMP NOT NULL DEFAULT now(), "course_id" integer NOT NULL, "file_id" integer NOT NULL, CONSTRAINT "PK_a2a2c9cac7ba893fdefa4db2f41" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "course" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "last_update_at" TIMESTAMP NOT NULL DEFAULT now(), "title" character varying(226) NOT NULL, "description" character varying NOT NULL, CONSTRAINT "UQ_ac5edecc1aefa58ed0237a7ee4a" UNIQUE ("title"), CONSTRAINT "PK_bf95180dd756fd204fb01ce4916" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "course"`);
        await queryRunner.query(`DROP TABLE "course-file"`);
    }

}
