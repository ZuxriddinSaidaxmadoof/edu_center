import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CaptchaModule } from '../captcha/captcha.module';
import { CaptchaRepository } from '../captcha/captcha.repository';
import { CaptchaService } from '../captcha/captcha.service';
import { CaptchaEntity } from '../captcha/entities/captcha.entity';
import { FileEntity } from '../file/entities/file.entity';
import { FileRepository } from '../file/file.repository';
import { FileService } from '../file/file.service';


@Module({
  imports: [TypeOrmModule.forFeature([FileEntity, CaptchaEntity])],
  providers: [ FileRepository, FileService, CaptchaService, CaptchaRepository ],
  exports: [ FileRepository, FileService, CaptchaService, CaptchaRepository ]
})
export class SharedModule {}
