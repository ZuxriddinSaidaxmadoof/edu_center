import { Cache } from '@nestjs/cache-manager';
import { Controller, Get, Post, Body, Patch, Param, Delete, Inject } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Auth } from 'src/common/decorators/Auth.decorator';
import { RoleEnum } from 'src/common/enums/enum';
import { CourseService } from './course.service';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';

@ApiTags("Course")
@Controller('course')
export class CourseController {
  constructor(private readonly courseService: CourseService, 
    @Inject("CACHE_MANAGER") private cacheManager: Cache
    ) {}

  @Auth(RoleEnum.SUPERADMIN)
  @Post()
  create(@Body() createCourseDto: CreateCourseDto) {
    return this.courseService.create(createCourseDto);
  }

  @Auth(RoleEnum.SUPERADMIN, RoleEnum.EMPLOYEE)
  @Get()
  async findAll() {
    const data =await this.courseService.findAll();
    
    return data;
  }

  @Auth(RoleEnum.SUPERADMIN, RoleEnum.EMPLOYEE)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.courseService.findOne(+id);
  }

  @Auth(RoleEnum.SUPERADMIN)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCourseDto: UpdateCourseDto) {
    return this.courseService.update(+id, updateCourseDto);
  }

  @Auth(RoleEnum.SUPERADMIN)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.courseService.remove(+id);
  }
}
