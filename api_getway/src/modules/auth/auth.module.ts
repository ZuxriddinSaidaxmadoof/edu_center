import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { join } from 'path';
import { ClientsModule } from '@nestjs/microservices';
import { Transport } from '@nestjs/microservices/enums';
import { USER_PACKAGE } from 'src/common/const/microservices';
import { JwtModule } from '@nestjs/jwt';
// import { CaptchaService } from 'src/lib/captcha.service';
import { SharedModule } from '../shared/shared.module';
import { CaptchaService } from '../captcha/captcha.service';
import { CaptchaRepository } from '../captcha/captcha.repository';

@Module({
  imports: [
    SharedModule,
    // JwtModule.register({
    //   global: true,
    //   secret: "ok",
    //   signOptions: { expiresIn: '1d' }
    // }),
    ClientsModule.register([
      {
        name: USER_PACKAGE,
        transport: Transport.GRPC,
        options: {
          package: 'auth',
          protoPath: join(__dirname, '../../proto/user.proto'),
          url: "localhost:7700"
        },
      },
    ])
  ],
  controllers: [AuthController],
  providers: [AuthService
    // {provide: "CaptchaService", useClass: CaptchaService}
  ],
})
export class AuthModule {}
