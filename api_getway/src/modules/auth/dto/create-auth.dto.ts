import { ApiProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty, IsEnum, IsUUID } from "class-validator";
import { RoleEnum } from "src/common/enums/enum";

export class CreateAuthDto {
    @ApiProperty({required: true, type: String, name: "login"})
    @IsString()
    @IsNotEmpty()
    login: string;
    @ApiProperty({required: true, type: String, name: "password"})
    @IsString()
    @IsNotEmpty()
    password: string;
    @ApiProperty({required: true, type: String, name: "role"})
    @IsEnum(RoleEnum)
    @IsNotEmpty()
    role: string;
    @ApiProperty({required: true, type: String, name: "captchaId"})
    @IsUUID()
    @IsNotEmpty()
    captchaId: string;
    @ApiProperty({required: true, type: String, name: "captchaAnswer"})
    @IsString()
    @IsNotEmpty()
    captchaAnswer: string;
}

export class LoginDto {
    @ApiProperty({required: true, type: String, name: "login"})
    @IsString()
    @IsNotEmpty()
    login: string;
    @ApiProperty({required: true, type: String, name: "password"})
    @IsString()
    @IsNotEmpty()
    password: string;
}
