export class AuthEntity {
    id: number;
    login: string;
    password: string;
    role: string;
    createdAt: Date;
    lastUpdatedAt: Date;
}
