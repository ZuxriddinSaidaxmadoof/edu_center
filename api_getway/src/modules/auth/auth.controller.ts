import { Cache } from '@nestjs/cache-manager';
import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Req, Inject, BadRequestException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ApiTags } from '@nestjs/swagger';
import { Auth } from 'src/common/decorators/Auth.decorator';
import { CurrentUser } from 'src/common/decorators/CurrentUser.decorator';
import { RoleEnum } from 'src/common/enums/enum';
import { ResData } from 'src/lib/resData';
import { CaptchaRepository } from '../captcha/captcha.repository';
import { CaptchaService } from '../captcha/captcha.service';
import { AuthService } from './auth.service';
import { CreateAuthDto, LoginDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { AuthEntity } from './entities/auth.entity';

@ApiTags('Auth')
@Controller('user')
export class AuthController {
  constructor(private readonly authService: AuthService,
    private readonly captchaService: CaptchaService,
    @Inject("CACHE_MANAGER") private cacheManager: Cache
    ) {}

  @Post("Sign-in")
  async login(@Body() createAuthDto: LoginDto) {
    const data =await this.authService.login(createAuthDto);
    data.subscribe(async(e) => {
      e.data ? 
      this.cacheManager.set(e.data?.data.id, e.data?.token).then() : null
    });
    return data;
  }

  @Post("Sign-up")
  async create(@Body() createAuthDto: CreateAuthDto) {
    const resultOfCaptcha = await this.captchaService.verifyCaptcha(createAuthDto.captchaAnswer, createAuthDto.captchaId);
    if(!resultOfCaptcha){
      return new ResData("Captcha answer wrong", 400)
    }
     
    const data =await this.authService.create(createAuthDto);
    return data
  }

  @Auth(RoleEnum.SUPERADMIN, RoleEnum.EMPLOYEE)
  @Post("Log-out")
  logout(@Req() req: Request) {
    console.log(req['user']);
    req['user'] = null;
    
    return new ResData("Loged out successfully", 200);
  }

  @Auth(RoleEnum.SUPERADMIN, RoleEnum.EMPLOYEE)
  @Get()
  findAll() {
    return this.authService.findAll()
  }

  @Auth(RoleEnum.SUPERADMIN, RoleEnum.EMPLOYEE)
  @Get('/get-me')
  getMe(@CurrentUser() currentUser: AuthEntity) {
    console.log(currentUser);

    return new ResData("User get-me", 200, currentUser)
  }

  @Auth(RoleEnum.SUPERADMIN)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.authService.findOne(+id)
  }

  @Auth(RoleEnum.SUPERADMIN)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAuthDto: UpdateAuthDto) {
    return this.authService.update(+id, updateAuthDto);
  }

  @Auth(RoleEnum.SUPERADMIN)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.authService.remove(+id);
  }
}
