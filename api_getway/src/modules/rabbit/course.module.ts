import { Module } from '@nestjs/common';
import { RabbitService } from './course.service';
import { CourseController } from './course.controller';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { RABBIT_PACKAGE } from 'src/common/const/microservices';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: RABBIT_PACKAGE,
        transport: Transport.GRPC,
        options: {
          package: 'rabbit',
          protoPath: join(__dirname, '../../proto/rabbit.proto'),
          url: "localhost:8800"
        },
      },
    ]),
  ],
  controllers: [CourseController],
  providers: [RabbitService],
  exports: [RabbitService]
})
export class RabbitModule {}
