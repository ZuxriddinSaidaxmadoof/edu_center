import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class CreateCourseDto {
    @ApiProperty({required: true, type: String, name: "tag"})
    @IsString()
    @IsNotEmpty()
    tag: string;
    @ApiProperty({required: true, type: String, name: "message"})
    @IsString()
    @IsNotEmpty()
    message: string;
}
