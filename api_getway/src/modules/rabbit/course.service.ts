import { Inject, Injectable } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { RABBIT_PACKAGE } from 'src/common/const/microservices';
import { CreateCourseDto } from './dto/create-course.dto';


@Injectable()
export class RabbitService {

  private courseService: any;

  constructor(@Inject(RABBIT_PACKAGE) private client: ClientGrpc,
  ) {}

  onModuleInit() {
    this.courseService = this.client.getService<any>('RabbitService');
  }


  async sendMessage(payload: CreateCourseDto) {
    const data = await this.courseService.send(payload)
    data.subscribe(() => {})

    return data;
  }
}
