import { Injectable } from '@nestjs/common';
import { join } from 'path';
import * as svgCaptcha from 'svg-captcha';
import { CreateCaptchaDto, ResponseCaptcha } from './dto/create-captcha.dto';
import * as fs from "fs";
import { config } from 'src/common/config';
import { v4 as uuid } from 'uuid';
import { CaptchaRepository } from './captcha.repository';
import { CaptchaEntity } from './entities/captcha.entity';
import { ResData } from 'src/lib/resData';
 
@Injectable()
export class CaptchaService {
  constructor(private readonly captchaRepository: CaptchaRepository){}

  async generateCaptcha():Promise<ResData<ResponseCaptcha>>{
    const captcha = svgCaptcha.createMathExpr({
        noise: 2, 
        mathMin: 1,
        mathMax: 10,
        size: 5, 
    });
    const fileName = `chaptcha${uuid()}image.svg`;
    fs.writeFile(join(__dirname, "../../../", "upload", fileName), captcha.data, (err) => {
        if (err) {
            console.error('Error writing SVG file:', err);
            return;
        }
        console.log('SVG file has been saved successfully.');
    });
    const fileLink = `http://localhost:${config.serverPort || 7777}/${fileName}`;
    const newCaptcha = new CreateCaptchaDto()
    newCaptcha.location = fileLink;
    newCaptcha.answer = captcha.text;
    
    const createdCaptcha = await this.captchaRepository.create(newCaptcha)
    return new ResData("Captcha generated", 201, {location: createdCaptcha.location, id: createdCaptcha.id});
  }

  async verifyCaptcha(userAnswer: string, captchId: string): Promise<boolean> {
    
      const findOne = await this.findOne(captchId);
      
      return userAnswer === findOne.data.answer;

      // await this.captchaRepository.delete(findOne)
  }


  async findOne(id: string):Promise<ResData<CaptchaEntity>> {
    const findCaptcha = await this.captchaRepository.findOneById(id)
    if(!findCaptcha){
      throw new ResData("captcha not found", 404);
    }
    return new ResData("One Captcha by id", 200, findCaptcha);
  }


  async deleteCaptcha(id: string):Promise<ResData<CaptchaEntity>> {
    const findCaptcha = await this.captchaRepository.findOneById(id)
    if(!findCaptcha){
      return new ResData("captcha not found", 404);
    }
    const deleted = await this.captchaRepository.delete(findCaptcha);
    return new ResData("Captcha deleted successfully", 200, deleted);
  }

}
