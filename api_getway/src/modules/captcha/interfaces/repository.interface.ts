import { CreateCaptchaDto } from "../dto/create-captcha.dto";
import { CaptchaEntity } from "../entities/captcha.entity"


export interface RepositoryInterface {
     findOneById(id: string): Promise<CaptchaEntity>;
     create(createCaptchaDto: CreateCaptchaDto): Promise<CaptchaEntity>;
}