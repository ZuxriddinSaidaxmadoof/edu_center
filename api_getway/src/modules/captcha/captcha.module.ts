import { Module } from '@nestjs/common';
import { CaptchaService } from './captcha.service';
import { CaptchaController } from './captcha.controller';
import { CaptchaRepository } from './captcha.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CaptchaEntity } from './entities/captcha.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([CaptchaEntity])
  ],
  controllers: [CaptchaController],
  providers: [CaptchaService, CaptchaRepository],
})
export class CaptchaModule {}
