import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreateCaptchaDto } from "./dto/create-captcha.dto";
import { CaptchaEntity } from "./entities/captcha.entity";
import { RepositoryInterface } from "./interfaces/repository.interface";

export class CaptchaRepository implements RepositoryInterface {
    constructor(
        @InjectRepository(CaptchaEntity)
        private usersRepository: Repository<CaptchaEntity>,
      ) {}

      async findOneById(id: string){
        return await this.usersRepository.findOneBy({id}); 
      }

      async create(createCaptchaDto: CreateCaptchaDto): Promise<CaptchaEntity>{
        const created = this.usersRepository.create(createCaptchaDto)
        return await this.usersRepository.save(created); 
      }

      async delete(captcha: CaptchaEntity): Promise<CaptchaEntity>{
        return await this.usersRepository.remove(captcha); 
      }
}