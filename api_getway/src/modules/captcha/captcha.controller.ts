import { Controller, Get, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CaptchaService } from './captcha.service';


@ApiTags("Capcha")
@Controller('capcha')
export class CaptchaController {
  constructor(private readonly captchaService: CaptchaService) {}

  @Post()
  getCaptcha() {
    return this.captchaService.generateCaptcha();
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.captchaService.findOne(id);
  // }
}
