export class CreateCaptchaDto {
    location: string;
    answer: string;
}

export class ResponseCaptcha {
    id: string;
    location: string;
}
