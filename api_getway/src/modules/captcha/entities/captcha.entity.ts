import { BeforeInsert, Column, Entity, PrimaryGeneratedColumn} from 'typeorm';
import {v4 as uuidv4} from "uuid"

@Entity('captcha')
export class CaptchaEntity{
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({type: 'text', nullable: false })
  location: string;

  @Column({ type: 'varchar', length: 32, nullable: false })
  answer: string;

  @BeforeInsert()
  generateId() {
    this.id = uuidv4();
  }
}

