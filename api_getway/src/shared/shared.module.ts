import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { redisStore } from 'cache-manager-redis-yet';
import { RABBIT_PACKAGE, USER_PACKAGE } from 'src/common/const/microservices';
import { AuthService } from 'src/modules/auth/auth.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { CaptchaEntity } from 'src/modules/captcha/entities/captcha.entity';
import { RabbitService } from 'src/modules/rabbit/course.service';



@Module({
  imports: [
    TypeOrmModule.forFeature([CaptchaEntity]),
  ClientsModule.register([
    {
      name: USER_PACKAGE,
      transport: Transport.GRPC,
      options: {
        package: 'auth',
        protoPath: join(__dirname, '../proto/user.proto'),
        url: "localhost:7700"
      },
    },
  ])
  ,
  ClientsModule.register([
    {
      name: RABBIT_PACKAGE,
      transport: Transport.GRPC,
      options: {
        package: 'rabbit',
        protoPath: join(__dirname, '../proto/rabbit.proto'),
        url: "localhost:8800"
      },
    },
  ]),

],
  providers: [
    AuthService, RabbitService
  ],
  exports: [
    AuthService, RabbitService
  ]
})
export class SharedModule {}
