import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';
import { AuthService } from 'src/modules/auth/auth.service';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly userService: AuthService,
    private jwtService: JwtService,
  ) {}

  async canActivate(context: ExecutionContext){    
    const request = context.switchToHttp().getRequest();
    const token = this.extractTokenFromHeader(request);
    if (!token) {
      throw new UnauthorizedException();
    }
    try {
      const payload = await this.jwtService.verifyAsync(token, {secret: "ok"})

      const foundUser = await this.userService.findOne(
        payload.id,
      );
      const resolved = new Promise<boolean>((resolve) => {
        foundUser
          .subscribe({
          next: value => request['user'] = value.data,
          complete: () => resolve(true), 
          })
      }); 
      return resolved.then(e => {
        return e;
      })
    } catch {
      throw new UnauthorizedException();
    }
  }

  private extractTokenFromHeader(request: Request): string | undefined {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
  }
}
