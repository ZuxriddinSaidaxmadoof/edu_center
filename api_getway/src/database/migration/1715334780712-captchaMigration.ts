import { MigrationInterface, QueryRunner } from "typeorm";

export class CaptchaMigration1715334780712 implements MigrationInterface {
    name = 'CaptchaMigration1715334780712'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "captcha" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "location" text NOT NULL, "answer" character varying(32) NOT NULL, CONSTRAINT "PK_f7c513ab5a7409973732468205b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "files" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "last_update_at" TIMESTAMP NOT NULL DEFAULT now(), "location" text NOT NULL, "name" character varying(256), "minetype" text NOT NULL, "size" integer NOT NULL, CONSTRAINT "PK_6c16b9093a142e0e7613b04a3d9" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "files"`);
        await queryRunner.query(`DROP TABLE "captcha"`);
    }

}
