import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { RabbitService } from 'src/modules/rabbit/course.service';
import { ResData } from './resData';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  constructor(private readonly httpAdapterHost: HttpAdapterHost, private readonly rabbitService: RabbitService) {}
  
  async catch(exception: any, host: ArgumentsHost): Promise<void> {
    const { httpAdapter } = this.httpAdapterHost;

    const ctx = host.switchToHttp();

    const responseBody = new ResData(
      '',
      HttpStatus.INTERNAL_SERVER_ERROR,
      null,
      exception,
    );

    if (exception instanceof HttpException) {
      responseBody.statusCode = exception.getStatus();
      
      const response = exception.getResponse() as Error;

      if (typeof response === 'string') {
        responseBody.message = response;
      } else {
        responseBody.message = response?.message.toString();
      }
    } else {
      responseBody.message = exception.message;
    }
    
    await this.rabbitService.sendMessage({tag: "apigetway", message: responseBody.message})
    httpAdapter.reply(ctx.getResponse(), responseBody, responseBody.statusCode);
  }
}
