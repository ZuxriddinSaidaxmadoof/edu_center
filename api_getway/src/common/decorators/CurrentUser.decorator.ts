import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const CurrentUser = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.user;
const response: Response = ctx.switchToHttp().getResponse()

    return data ? user?.[data] : user;
  },
);
