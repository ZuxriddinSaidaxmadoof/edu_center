export enum RoleEnum {
  SUPERADMIN = 'superAdmin',
  EMPLOYEE = 'employee',
}

export enum StatusEnum {
  CREDIT = 'credit',
  DEBIT = 'debit'
}

export enum StatusTrack {
  CREATED = 'created',
  PROGRESS = 'progress',
  DONE = 'done'
}
