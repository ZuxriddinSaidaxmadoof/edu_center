import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { Response } from 'express';

interface ErrorResponse<TData, TError> {
  message: string;
  statusCode: number;
  data: TData | null;
  error: TError | null;
}

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    
    let statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    let message = 'Internal server error';
    let data: any = null;
    let error: any = null;

    if (exception instanceof HttpException) {
      statusCode = exception.getStatus();
      message = exception.message;
      data = exception.getResponse();
    } else if (exception instanceof RpcException) {
      statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
      message = exception.message;
    }

    response.status(statusCode).json({
      message,
      statusCode,
      data,
      error,
    });
  }
}