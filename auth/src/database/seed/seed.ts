import { createConnection, DataSource } from 'typeorm';
import { RoleEnum } from 'src/common/enums/enum';
import { typeOrmConfig } from 'src/common/database/config';
import { UserEntity } from 'src/modules/users/entities/user.entity';

(async () => {
  const datasource: DataSource = await createConnection(typeOrmConfig);

  const queryRunner = datasource.createQueryRunner();

  await queryRunner.connect();
  await queryRunner.startTransaction();
  try {
    const userRepository = queryRunner.manager.getRepository(UserEntity);

    const users = await userRepository.find();
    await userRepository.remove(users);

    const admin = {
      login: 'admin',
      password: 'admin',
      role: RoleEnum.SUPERADMIN,
    };

    let user1 = userRepository.create(admin);
    user1 = await userRepository.save(user1);

    const employee = {
      login: 'employee',
      password: 'employee',
      role: RoleEnum.EMPLOYEE,
    };

    let user2 = userRepository.create(employee);
    user2 = await userRepository.save(user2);

    await queryRunner.commitTransaction();
  } catch (err) {
    await queryRunner.rollbackTransaction();
  } finally {
    await queryRunner.release();
  }
})();
